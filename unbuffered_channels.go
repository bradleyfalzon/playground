// This program demonstrates a potential use case of using unbuffered channels
// to execute two unrelated tasks concurrently, and then wait for both of the
// results before continuing.
//
// This could have also used sync.WaitGroup, but in the real world, we'd only
// block whilst we're waiting for a specific channel to return to do next task.
package main

import (
	"log"
	"time"
)

func main() {

	started := time.Now()

	// Make the channels the routines will send data to
	sql1 := make(chan int)
	sql2 := make(chan int)

	var sleepTime1 uint = 3
	var sleepTime2 uint = 10

	// Start first goroutine that does some stuff quickly
	go func(sleepTime uint) {
		log.Printf("sql1: Starting to sleep for %d seconds", sleepTime)
		time.Sleep(time.Duration(sleepTime) * time.Second)
		log.Println("sql1: Finished sql1, sending result")
		sql1 <- 1 // Send the result to channel.
		log.Println("sql1: exiting")
	}(sleepTime1)

	// Start second goroutine that does some stuff but slower
	go func(sleepTime uint) {
		log.Printf("sql2: Starting to sleep for %d seconds", sleepTime)
		time.Sleep(time.Duration(sleepTime) * time.Second)
		log.Println("sql2: Finished sql2, sending result")

		sql2 <- 2 // Send the result to channel.
		log.Println("sql2: exiting")
	}(sleepTime2)

	log.Println("main: Waiting for queries to return")
	log.Printf("main: sql1=%v sql2=%v\n", <-sql1, <-sql2)
	log.Printf("main: Finished in %s, exiting...", time.Since(started))
}
