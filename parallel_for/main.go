package main

import (
	"fmt"
	"sync"
)

func main() {

	var wg sync.WaitGroup

	elements := []string{"el0", "el1", "el2", "el3", "el4"}

	fmt.Println("This is the wrong way, the for loop overwrites i over each iteration")
	for i := range elements {
		wg.Add(1)
		go func() {
			defer wg.Done()
			fmt.Println("i:", i)
		}()
	}

	// Sleep for a moment as the main thread needs to wait for the go routines
	wg.Wait()

	fmt.Println("This is the right way, each go routine receives a copy of i")
	for i := range elements {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			fmt.Println("i:", i)
		}(i)
	}

	// Sleep for a moment as the main thread needs to wait for the go routines
	wg.Wait()

	fmt.Println("Lets change the elements concurrently")
	for i := range elements {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			elements[i] += "-new"
		}(i)
	}

	// Sleep for a moment as the main thread needs to wait for the go routines
	wg.Wait()

	for i, k := range elements {
		fmt.Printf("i: %d, k: %s\n", i, k)
	}

}
