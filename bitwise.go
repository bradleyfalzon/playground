package main

import "fmt"

func main() {

	val := 5
	fmt.Println("Value:", val)

	// this seems to be a pattern
	// int(b[0]&0x0f) << 2

	// shifts right and pads left with zeros
	// Makes 00000101
	// To 00000010
	fmt.Println("Right shift:", val>>1)

	// shifts left and pads right with zeros
	// Makes 00000101
	// to 0000001010
	fmt.Println("Left shift:", val<<1)

}
