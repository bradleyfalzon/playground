package main

import "net"
import "log"

// Very simple example of listening on a udp port
// Send data via netcat: echo -n "hello" | nc -4u -w1 127.0.0.1 1234
func main() {
	listenAddr := "127.0.1:1234"

	serverAddr, err := net.ResolveUDPAddr("udp", listenAddr)
	if err != nil {
		log.Fatal(err)
	}

	conn, err := net.ListenUDP("udp", serverAddr)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Listening on", listenAddr)

	for {
		b := make([]byte, 2048)
		n, addr, err := conn.ReadFromUDP(b)
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Received %d bytes from %s", n, addr)
		log.Printf("Raw: %#v", b[:n])
		log.Printf("String: %s", string(b[:n]))

	}
}
