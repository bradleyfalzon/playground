// This program is an example of have a series of tasks to complete, and executing
// them in parallel, ignoring the actual results (although it is possible to also
// return the result for later).
package main

import (
	"log"
	"sync"
	"time"
)

func main() {

	var (
		workers       int = 2
		channelBuffer int = 50
	)
	//jobs := []int{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}
	jobs := []int{2, 2, 2, 2, 2, 2, 2, 2, 2, 2}

	started := time.Now()
	log.Printf("main: Setting up %d workers, channel buffer %d, jobs %d", workers, channelBuffer, len(jobs))

	// Create our channel, but add channelBuffer to indicated it's a buffered channel of size channelBuffer
	channel := make(chan int, channelBuffer)

	// Send data to the routines, note: this needs to be in a goroutine itself *in case* there's
	// more jobs that can fit inside the channelBuffer. If len(jobs) < channelBuffer, then there's
	// no real reason it needs to be in a goroutine itself.
	go func(jobs []int) {
		for _, val := range jobs {
			log.Println("initial: Adding jobs:", val)
			channel <- val
		}
		log.Printf("initial: finished adding %d jobs to buffered channel", len(jobs))
	}(jobs)

	// Create a wait group. We'll then increment this waitgroup as we add more
	// workers. Then, as the workers finish, they'll mark themselves as done.
	// Our main routine will wait until they're all done before exiting.
	var wg sync.WaitGroup

	// Start a number of workers
	for i := 0; i < workers; i++ {
		log.Println("main: Starting worker number", i)

		// Increment the waitgroup counter so we know how many matching Done()s we need
		// Alternatively we could've specified exactly how many workers we were going to
		// create before the for loop.
		//
		// Must also be done outside the goroutine to ensure we don't start waiting before
		// all the increments have been made
		wg.Add(1)
		go func(i int, wg *sync.WaitGroup) {
			// Defer a notify to the waitgroup we're finished
			defer wg.Done()
			log.Printf("routine %d: Routine started, waiting for data", i)
			for {
				select {
				case data := <-channel:
					log.Printf("routine %d: Routine received: %v", i, data)
					time.Sleep(time.Duration(data) * time.Second)
				default:
					log.Printf("routine %d: Nothing more to process, exiting...", i)
					return
				}
				log.Printf("routine %d: Routine finished processing", i)
			}
		}(i, &wg)
	}

	log.Println("main: Waiting for waitgroup")
	wg.Wait()
	log.Printf("main: Wait group finished in %s, exiting...", time.Since(started))

}
